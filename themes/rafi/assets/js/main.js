jQuery(function($) {

    'use strict';

    var _Blog = window._Blog || {};

    _Blog.prettify = function() {
        $('pre').addClass('prettyprint linenums').attr('style', 'overflow:auto;');
        window.prettyPrint && prettyPrint();
    };

    _Blog.externalUrl = function() {
        $.expr[':'].external = function(obj) {
            return !obj.href.match(/^mailto\:/) &&
                (obj.hostname != location.hostname);
        };
        $('a:external').addClass('external');
        $(".external").attr('target', '_blank');

    }

    _Blog.toggleTheme = function() {
        const currentTheme = window.localStorage && window.localStorage.getItem('theme')
        const isDark = currentTheme === 'dark'
        $('.theme-switch').on('click', () => {
            $('body').toggleClass('dark-theme')
            if (document.body.classList.contains('dark-theme')){
                window.localStorage && window.localStorage.setItem('theme', 'dark')
                $('meta[name=theme-color]').attr('content', '#292a2d');
            }else{
                window.localStorage && window.localStorage.setItem('theme', 'light')
                $('meta[name=theme-color]').attr('content', '#ffffff');
            }
        })

        if (currentTheme !== null){
            if (!isDark){
                $('body').toggleClass('dark-theme')
            }
        }else{
            if (!(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                $('body').toggleClass('dark-theme')
            }
        }

    }

    _Blog.toggleMobileMenu = function() {
        $('.menu-toggle').on('click', () => {
            $('.menu-toggle').toggleClass('active')
            $('#mobile-menu').toggleClass('active')
        })
    }

    $(document).ready(function() {
        _Blog.prettify()
        _Blog.toggleTheme()
        _Blog.toggleMobileMenu()
    });
});