var prevScrollpos = window.pageYOffset;
var navDesktop = document.getElementById("nav-desktop");
var navMobile = document.getElementById("nav-mobile");

window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    navDesktop.style.top = "0";
  } else {
    navDesktop.style.top = "-4rem";
  }

  if (prevScrollpos > currentScrollPos) {
    navMobile.style.top = "0";
  } else {
    navMobile.style.top = "-5rem";
  }
  
  prevScrollpos = currentScrollPos;
}