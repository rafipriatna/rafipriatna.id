---
title: "Inventory VB"
slug: "inventory-vb"
thumbnail: "images/works/vs.png"
---

![Desktop](toto.PNG)

Dibuat : Juni 2019

Source : Close Source

Teknologi : VB.NET, MySQL

Keterangan : Aplikasi yang berjalan di sistem operasi Windows 10. Aplikasi ini berguna untuk memanajemen inventori perusahaan.
*Tidak ada keterkaitan dengan perusahaan Toto. Perusahaan tersebut yang tertulis pada aplikasi ini hanya sebagai penunjang skripsi.