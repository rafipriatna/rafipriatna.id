---
title: "POS PHP"
slug: "point-of-sales-php"
thumbnail: "images/works/php.png"
---

![Desktop](php_pos.PNG)

Dibuat : Juni 2018

Source : [Open Source](https://github.com/rafipriatna/POS-PHP)

Teknologi : PHP, MySQL

Keterangan : Aplikasi berbasis web untuk melakukan penjualan barang. Aplikasi ini bisa melakukan scan barang dengan barcode, dan juga bisa membuat laporan.