---
title: "LaraPOS"
slug: "larapos"
thumbnail: "images/works/laravel.png"
---

![Desktop](larapos.png)

Dibuat : Agustus 2020

Source :  [Open Source](https://github.com/rafipriatna/LaraPOS)

Teknologi : PHP, Laravel, MySQL, Bootstrap

Keterangan : Aplikasi penjualan sederhana yang dibuat dengan Laravel 7.

Fitur-fiturnya :
- CRUD User
- CRUD Products
- CRUD Customers
- CRUD Coupons
- Halaman Profile Toko
- Halaman Transaksi
- Laporan Transaksi
