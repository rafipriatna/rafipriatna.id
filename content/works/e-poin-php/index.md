---
title: "e-Poin PHP"
slug: "e-poin-php"
thumbnail: "images/works/php.png"
---

![Desktop](php_epoin.PNG)

Dibuat : Juli 2018

Source : [Open Source](https://github.com/rafipriatna/e-Poin)

Teknologi : PHP, MySQL

Keterangan : Aplikasi berbasis web untuk memanajemen data pelanggaran siswa atau siswi dengan menambahkan poin.