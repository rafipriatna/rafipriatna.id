---
title: "About Me"
slug: "about"
menu: "main"
---

Saya Rafi, seorang mahasiswa jurusan Sistem Informasi. Saya biasanya membuat projek open-source
yang saya simpan di [Github saya](https://github.com/rafipriatna).

Saya membuat blog ini dengan tujuan untuk mendokumentasikan hasil pembelajaran saya. Semoga isi blog ini
berguna untuk para pembaca sekalian. :D

### Saya Menggunakan

- **Laptop:** Acer Swift 3 SF314-54G
- **Sistem Operasi:** Linux Mint 20  
- **Domain:** [Domainesia](https://www.domainesia.com/?aff=6794)
- **Hosting:** [Gitlab Pages](https://gitlab.com)
- **Editor / IDE:** [Visual Studio Code](https://code.visualstudio.com/), [Visual Studio Community](https://visualstudio.microsoft.com/vs/community/), [Android Studio](https://developer.android.com/studio)

### Hubungi Saya
Ingin bertanya atau hanya sekedar menyapa? Let's chats with me :D
- Email     : [me@rafipriatna.id](#)
- Twitter   : [@rafipriatna](https://twitter.com/rafipriatna)
- Instagram : [@rafipriatna](https://instagram.com/rafipriatna)
- Telegram  : [@rafipriatna](https://t.me/rafipriatna)